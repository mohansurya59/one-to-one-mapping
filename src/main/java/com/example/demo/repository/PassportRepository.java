package com.example.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Passport;
import com.example.demo.model.Student;

public interface PassportRepository extends CrudRepository<Passport, Integer> {
	
	@Query("select s from Student s where id= :studentid")
	public Student getStudent(int studentid);

}
